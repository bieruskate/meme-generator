"""
Text preprocessor for meme text
"""
import re
import subprocess


class WCRFTTextPreprocessor:
    WORD_REGEX = re.compile(r'[^a-zA-ZąęźłśćżóńĄĘŹŻŁŚĆÓŃ]')

    def __init__(self):
        self._word_exceptions = {
            'sie': 'się'
        }

    def run(self, text):
        tagged = run_tagger(text)
        lemmas, gram_categories = self._parse_tagged_texts(tagged)
        
        return lemmas, gram_categories

    def _parse_tagged_texts(self, tagger_out):
        lemmas, gram_cat = [], []

        for line in tagger_out.split('\n'):
            if not line:
                continue

            base_word, lemma, tags = line.strip().split('\t')

            if len(base_word) < 2:
                continue

            if self.WORD_REGEX.match(base_word):
                continue

            tag = tags.split(':')[0] if ':' in tags else tags

            lemmas.append(lemma.lower())
            gram_cat.append(tag)

        return lemmas, gram_cat


def run_tagger(text):
    cmd = [
        'wcrft', 'nkjp_e2.ini',
        '-i', 'txt', '-',
        '-o', 'iob-chan',
        '-d', '/usr/local/lib/python2.7/dist-packages/wcrft-1.0.0-py2.7.egg/wcrft/model/model_nkjp10_wcrft_e2/',
    ]

    proc = subprocess.Popen(' '.join(cmd), shell=True,
                            stdin=subprocess.PIPE, stdout=subprocess.PIPE)
    stdout, _ = proc.communicate(input=text.encode('utf-8'))
    return stdout.decode('utf-8')

