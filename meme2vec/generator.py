import pickle

from keras import models as ks_mod
from keras.preprocessing import sequence as ks_seq
import numpy as np
import tensorflow as tf

from meme2vec import preprocessor as m2v_pre


class Generator:
    PADDING_LEN = 16
    MEME_TEMPLATES_MAPPINGS = {
         0: ('Chuck Norris', 'chuck.png'),
         1: ('Filozoraptor', 'filozoraptor.png'),
         2: ('Forever Alone', '4everalone.png'),
         3: ('Gruba dziewczyna', 'gruba.png'),
         4: ('Pechowiec Brian', 'brian.png'),
         5: ('Suchy Karol', 'suchy-karol.png'),
         6: ('Wielkodupny no-life', 'no-life.png'),
         7: ('Y U NO', 'yuno.png'),
         8: ('ZSRR', 'zsrr.png'),
         9: ('Zjarany Zbyszek', 'zbyszek.png')
    }

    def __init__(self, word_mappings_file, grammar_mappings_file,
                 nn_model_file):
        self._word_mappings = None 
        self._grammar_mappings = None
        self._nn_model = None
        self._graph = None
        
        self._load_model(nn_model_file)
        self._load_mappings(word_mappings_file, grammar_mappings_file)

    def _load_model(self, model_file):
        self._nn_model = ks_mod.load_model(model_file)
        self._graph = tf.get_default_graph()

    def _load_mappings(self, word_mappings_file, grammar_mappings_file):
        embs = np.load(word_mappings_file).item()
        self._word_mappings = embs['mappings']
        
        with open(grammar_mappings_file, 'rb') as fh:
            self._grammar_mappings = pickle.load(fh)

    def predict_template(self, text):
        wcrft = m2v_pre.WCRFTTextPreprocessor()
    
        lemmas, gram_cls = wcrft.run(text)
    
        lemmas = self.map_words(lemmas)
        gram_cls = self.map_grammar_classes(gram_cls)
    
        with self._graph.as_default():
            pred_softmax = self._nn_model.predict([lemmas, gram_cls])

        label = np.argmax(pred_softmax)
        _, template = self.MEME_TEMPLATES_MAPPINGS[label]

        return template 

    def map_words(self, text):
        sequence = [self._word_mappings.get(word, -1) + 1 for word in text]
        padded_seq = ks_seq.pad_sequences([sequence], maxlen=self.PADDING_LEN)
        return padded_seq

    def map_grammar_classes(self, gram_cls):
        gram_cls = [self._grammar_mappings[cls] for cls in gram_cls]
        padded_seq = ks_seq.pad_sequences([gram_cls], maxlen=self.PADDING_LEN)
        return padded_seq



