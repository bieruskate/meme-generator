function getCaptions() {
  return $('.mg-textbox input')
          .map((ind, input) => input.value)
          .get();
}

function getCaption() {
  return getCaptions().join(' ').trim();
}

function setImageTemplate(template) {
  $('#meme').attr('src', 'templates/' + template);
}

function updateTemplate() {
  var caption = getCaption();

  if (caption === '') {
    setImageTemplate('initial.png')
  }
  else {
    $('#loader').css('display', 'block');
    setImageTemplate('initial.png');

    $.ajax({
      url: '/api/meme-template',
      type: 'POST',
      contentType: "application/json; charset=utf-8",
      dataType: 'json',
      data: JSON.stringify({text: caption.replace(' ', '%20')}),
      success: (res) => {
        $('#loader').css('display', 'none');
        setImageTemplate(res.template);
        makeMemeGenerator(getCaptions());
      },
    });
  }
}


var memeGenerator = null;

function makeMemeGenerator(captions) {
  if (!!memeGenerator) {
    memeGenerator.destroy();
  }

  $('#meme').memeGenerator({
    dragResizeEnabled: false,
    showAdvancedSettings: false,
    captions: captions,
    onInit: function() {
      memeGenerator = this;
    }
  });

}


$(document).ready(function(){
  makeMemeGenerator([]);

  $("#generate-button").on("click", () => updateTemplate());

  $('#loader').css('display', 'none');
});
