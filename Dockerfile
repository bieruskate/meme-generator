FROM clarinpl/nlptools:latest

RUN apt-get update && apt-get install -y python3 python3-pip nginx

COPY data/default.conf /etc/nginx/sites-available/default

COPY requirements.txt /tmp/requirements.txt
RUN pip3 install -r /tmp/requirements.txt

WORKDIR /project

EXPOSE 80
CMD ["sh", "-c", "nginx; python3 server.py"]
